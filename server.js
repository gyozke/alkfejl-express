const express = require('express');
const nunjucks = require('nunjucks');
const bodyParser = require('body-parser');
const indicative = require('indicative');
const session = require('express-session');
const flash = require('connect-flash');

// The express application
const app = express();

// Configuring the template engine
nunjucks.configure('views', {
    autoescape: true,
    express: app
});

// Middlewares
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(session({
    cookie: { maxAge: 600000 }, // 10 minutes
    secret: 'secret code',
    resave: false,
    saveUninitialized: false,
}));
app.use(flash());

app.use(function (req, res, next) {
  console.log(req.method, req.url);
  next();
});

// Endpoints
app.get('/', function (req, res) {
  res.render('page.njk', { username: 'Anonymous' })
});

app.get('/alma', function (req, res) {
  res.render('alma.njk', { username: 'Anonymous' })
});

app.get('/hello/:name', function (req, res) {
  const name = req.params.name;
  res.render('hello.njk', { name })
});

app.get('/bgcolor', function (req, res) {
  const bgcolor = req.query.bgcolor;
  res.render('bgcolor.njk', {bgcolor} )
});

app.get('/reg', function (req, res) {
  const errors = req.flash('errors');
  const old_data = req.flash('old_data');

  // console.log(errors);

  res.render('reg.njk', {
    errors,
    old_data,
  });
});

app.post('/reg', function (req, res) {
  // console.log(req.body);
  // const {username,password,password_confirm,email} = req.body;

  const rules = {
    username          : 'required|alpha_numeric|min:6|max:20',
    email             : 'required|email',
    password          : 'required|alpha_numeric|min:6|max:20',
    password_confirm  : 'same:password',
  };

  indicative
    .validateAll(req.body, rules)
    .then (function () {
      // validation passed
      res.redirect('/alma');
      res.end('success');
    })
    .catch(function (errors) {
      // validation failed
      // console.log(errors);
      req.flash('errors', errors);
      req.flash('old_data', req.body);
      res.redirect('/reg');
      res.end();
    });
});

// Starting the server
const port = process.env.PORT || 3000;
const server = app.listen(port, function () {
  console.log('Server is running!');
});