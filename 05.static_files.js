var express = require('express');
var app = express();

app.use(express.static('public'));

app.use(function (req, res, next) {
  console.log(req.method, req.url);
  next();
});

var port = process.env.PORT || 3000;
var server = app.listen(port, function () {
  console.log('Server is running!');
});