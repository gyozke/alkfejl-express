const express = require('express');
const nunjucks = require('nunjucks');

// The express application
const app = express();

// Configuring the template engine
nunjucks.configure('views', {
    autoescape: true,
    express: app
});

// Middlewares
app.use(express.static('public'));

app.use(function (req, res, next) {
  console.log(req.method, req.url);
  next();
});

// Endpoints
app.get('/', function (req, res) {
  res.render('page.njk', { username: 'Anonymous' })
});

app.get('/alma', function (req, res) {
  res.render('alma.njk', { username: 'Anonymous' })
});

app.get('/hello/:name', function (req, res) {
  const name = req.params.name;
  res.render('hello.njk', { name })
});

// Starting the server
const port = process.env.PORT || 3000;
const server = app.listen(port, function () {
  console.log('Server is running!');
});