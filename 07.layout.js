var express = require('express');
var nunjucks = require('nunjucks');

// The express application
var app = express();

// Configuring the template engine
nunjucks.configure('views', {
    autoescape: true,
    express: app
});

// Middlewares
app.use(express.static('public'));

app.use(function (req, res, next) {
  console.log(req.method, req.url);
  next();
});

// Endpoints
app.get('/', function (req, res) {
  res.render('page.njk', { username: 'Anonymous' })
});

app.get('/alma', function (req, res) {
  res.render('alma.njk', { username: 'Anonymous' })
});

// Starting the server
var port = process.env.PORT || 3000;
var server = app.listen(port, function () {
  console.log('Server is running!');
});